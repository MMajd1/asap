import React from 'react';
import {SafeAreaView} from 'react-native';
import {AppNavigation} from './src/navigation';
import {colors} from './src/utils';
import {styles} from './src/utils/styles';

const App = () => (
  <SafeAreaView
    style={[styles.container, {backgroundColor: colors.backgroundGray}]}>
    <AppNavigation />
  </SafeAreaView>
);
export default App;
