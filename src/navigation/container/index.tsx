import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {MainStack, navigationRef} from '../index';

export const AppNavigation = () => (
  <NavigationContainer ref={navigationRef}>
    <MainStack />
  </NavigationContainer>
);
