import React, {memo} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {getHeaderTitle} from '@react-navigation/elements';
import {Home} from '../../screens';
import {colors, constants, routes, scale} from '../../utils';
import {Models, ModelDetails} from '../../screens';
import {wording} from '../../utils';
import {StackHeader} from '../../components/stack-header';
import {StackHeaderButton} from '../../components';

const Stack = createNativeStackNavigator();

export const MainStack = memo(() => (
  <Stack.Navigator
    screenOptions={{
      header: ({navigation, route, options, back}) => {
        const title = getHeaderTitle(options, route.name);
        return (
          <StackHeader
            title={title}
            leftButton={
              back ? (
                <StackHeaderButton
                  text={wording.back}
                  onPress={navigation.goBack}
                />
              ) : undefined
            }
            style={options.headerStyle}
          />
        );
      },
    }}>
    <Stack.Group>
      <Stack.Screen
        component={Home}
        name={routes.Home.name}
        options={{title: wording.homeTitle}}
      />
      <Stack.Screen
        name={routes.Model.name}
        component={Models}
        options={{title: wording.modelTitle}}></Stack.Screen>
      <Stack.Screen
        name={routes.ModelDetails.name}
        component={ModelDetails}
        options={{title: wording.modelDetailsTitle}}></Stack.Screen>
    </Stack.Group>
  </Stack.Navigator>
));
