import React, {memo, useState} from 'react';
import {SearchField} from '../../components';
import {ProductsList} from '../../components/products-list';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../utils';

export const Models = memo(({}) => {
  const [searchValue, setSearchValue] = useState('');
  const products = useSelector(state => state.products);

  return (
    <>
      <SearchField
        value={searchValue}
        onChangeText={text => setSearchValue(text)}
        style={{backgroundColor: colors.white}}
      />
      <ProductsList
        products={products.filter(({title}) =>
          title.toLowerCase().includes(searchValue),
        )}
      />
    </>
  );
});
