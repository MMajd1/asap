import React, {memo, useState} from 'react';
import {Image, ScrollView, StyleSheet, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  Comment,
  CustomButton,
  CustomTextInput,
  Feature,
} from '../../components';
import {Accordion} from '../../components/accordion';
import {addComment} from '../../store/product-slice';
import {colors, scale, wording} from '../../utils';
import {styles} from '../../utils/styles';

export const ModelDetails = memo(({route: {params}, navigation}) => {
  const [commentInputValue, setCommentInputValue] = useState('');
  const {id} = params;
  const dispatch = useDispatch();
  const product = useSelector(state => state.products.find(p => p.id == id));
  const {features, comments} = product;

  return (
    <ScrollView>
      <View style={[styles.column, detailsStyles.container]}>
        <View style={[styles.centered, detailsStyles.imageContainer]}>
          <Image
            style={detailsStyles.image}
            source={require('../../../images/default-image.png')}
          />
        </View>
        <View style={{marginTop: scale(20), width: '100%'}}>
          <Accordion title={wording.imageInfo} isOpen={true}>
            <>
              {features.map(({id, title, value}) => (
                <Feature key={id} title={title} value={value} />
              ))}
            </>
          </Accordion>
          <Accordion title={wording.notes}>
            <View style={{alignItems: 'center'}}>
              <View style={styles.column}>
                <CustomButton
                  title={wording.save}
                  style={{
                    alignSelf: 'flex-end',
                    marginBottom: scale(10),
                  }}
                  textStyle={{fontSize: scale(12)}}
                  onPress={() => {
                    if (commentInputValue.trim().length) {
                      dispatch(
                        addComment({id, comment: commentInputValue.trim()}),
                      );
                      setCommentInputValue('');
                    }
                  }}
                />
                <CustomTextInput
                  placeholder={wording.addNote}
                  style={{
                    flex: 0,
                    fontSize: scale(15),
                    backgroundColor: colors.white,
                    width: scale(312),
                    height: scale(38),
                    borderRadius: scale(19),
                    alignSelf: 'center',
                  }}
                  value={commentInputValue}
                  onChangeText={text => setCommentInputValue(text)}
                />
              </View>
              <View style={detailsStyles.history}>
                <View style={styles.column}>
                  {comments.map((item, index) => (
                    <Comment key={item.value} index={index} comment={item} />
                  ))}
                </View>
              </View>
            </View>
          </Accordion>
        </View>
      </View>
    </ScrollView>
  );
});

const detailsStyles = StyleSheet.create({
  container: {
    paddingHorizontal: scale(20),
    paddingVertical: scale(20),
    marginHorizontal: scale(20),
    marginVertical: scale(20),
    borderRadius: scale(25),
    alignItems: 'center',
    backgroundColor: colors.componentsGray,
  },
  imageContainer: {
    backgroundColor: colors.white,
    borderRadius: scale(19),
    width: scale(224),
    height: scale(163),
  },
  image: {
    width: scale(196),
    height: scale(130),
    resizeMode: 'contain',
  },
  history: {
    backgroundColor: colors.white,
    width: scale(312),
    alignSelf: 'center',
    marginTop: scale(20),
    paddingHorizontal: scale(20),
    paddingVertical: scale(20),
    borderRadius: scale(20),
  },
  rowSpaceBetween: {flexDirection: 'row', justifyContent: 'space-between'},
});
