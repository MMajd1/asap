import React, { memo } from 'react';
import { InteractionManager, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { NavButton } from '../../components/nav-button';
import { navigate, navigationRef } from '../../navigation';
import { INavItem } from '../../types';
import { routes, scale } from '../../utils';
import { styles } from '../../utils/styles';

const NavLinks = [
  {
    key: 'some-arnadomakdjfsd',
    title: 'Asset Inventory',
  },
  {
    key: 'asdlfkj-asdfkj',
    title: 'Model',
  },
  {
    key: 'asdlfkjaksldjfkasdjf-asdfkj',
    title: 'Person',
  },
] as INavItem[];

export const Home = memo(() => (
  <View style={[styles.container, styles.column, { marginTop: scale(20), paddingHorizontal: scale(30) }]}>
    <FlatList
      data={NavLinks}
      renderItem={({ item }: { item: INavItem }) => (
        <NavButton
          key={item.key}
          title={item.title}
          onPress={() => InteractionManager.runAfterInteractions(() => navigate(routes.Model.name))}
        />
      )}
    />
  </View>
));
