export {scale, verticalScale, horizontalScale} from './dimensions';
export {colors} from './colors';
export {routes} from './routes';
export {constants} from './constants';
export {default as wording} from './wording';
