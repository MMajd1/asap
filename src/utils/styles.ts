import { StyleSheet } from 'react-native';
import { colors, constants, scale } from '.';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: colors.backgroundGray,
  },
  row: {
    flexDirection: 'row',
  },
  column: {
    flexDirection: 'column',
  },
  centered: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBig: {
    fontSize: scale(18),
  },
  textMid: {
    fontSize: scale(11),
  },
  textSmall: {
    fontSize: scale(6),
  },
  textBold: {
    fontWeight: 'bold',
  },
  textItalic: {
    fontStyle: 'italic',
  },
  boxShadow: {
    shadowColor: colors.lightGray,
    shadowOffset: {
      width: 0,
      height: scale(1),
    },
    shadowOpacity: 0.01,
    shadowRadius: scale(2),
    elevation: scale(3),
  },
  seperator: {
    borderBottomColor: colors.lightGray,
    borderBottomWidth: 1,
    flex: 1,
  },
});
