import {scale} from '.';

export const constants = {
  product: {
    width: scale(157),
    height: scale(135),
    imageContainer: {
      width: scale(157),
      height: scale(114),
    },
    image: {
      width: scale(112),
      height: scale(112),
    },
    textContainer: {
      width: scale(157),
      height: scale(21),
      fontSize: scale(11),
    },
  },
  headerHeight: scale(53),
};
