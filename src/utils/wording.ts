export default {
  save: 'Save',
  modelTitle: 'Models',
  homeTitle: 'Picture',
  modelDetailsTitle: 'Model Details',
  notes: 'Notes',
  addNote: 'Add Notes',
  imageInfo: 'Image Info',
  back: 'Back',
  typeToSearch: 'Type to search'
};
