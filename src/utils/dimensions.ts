import { Dimensions, PixelRatio } from 'react-native';

export const { width: DEVICE_WIDTH, height: DEVICE_HEIGHT } = Dimensions.get('window');
export const guidelineBaseWidth = 375;
export const guidelineBaseHeight = 812;
export const horizontalScale = (size) => (DEVICE_WIDTH / guidelineBaseWidth) * size;
export const verticalScale = (size) => (DEVICE_WIDTH / guidelineBaseHeight) * size;
export const scale = (size) => {
  return PixelRatio.roundToNearestPixel((size - 1) * (DEVICE_HEIGHT / guidelineBaseHeight));
};
