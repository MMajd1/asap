import {configureStore} from '@reduxjs/toolkit';
import productsReducer from './product-slice';

export default configureStore({
  reducer: {
    products: productsReducer,
  },
});
