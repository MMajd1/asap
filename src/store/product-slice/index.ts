import {createSlice} from '@reduxjs/toolkit';
import {IProduct} from '../../types';

const intialProducts = [
  {
    id: 'adsoijasdfj',
    imageUrl: 'aoasd',
    title: 'Product ' + Math.floor(Math.random() * 100),
    features: [
      {
        id: Math.floor(Math.random() * 10000000),
        title: 'Size',
        value: '1209*320',
      },
      {
        id: Math.floor(Math.random() * 10000000),
        title: 'Size',
        value: '1209*320',
      },
    ],
    comments: [
      {
        name: 'Jon Doe',
        value: 'Quality Product',
      },
    ],
  },
  {
    id: 'askdfj902390234',
    imageUrl: 'aoasd',
    title: 'Product ' + Math.floor(Math.random() * 100),
    features: [
      {
        id: Math.floor(Math.random() * 10000000),
        title: 'Size',
        value: '1209*320',
      },
      {
        id: Math.floor(Math.random() * 10000000),
        title: 'Size',
        value: '1209*320',
      },
    ],
    comments: [
      {
        name: 'Jon Doe',
        value: 'Quality Product',
      },
    ],
  },
  {
    id: '2039u4kajsdfadsf',
    imageUrl: 'aoasd',
    title: 'Product ' + Math.floor(Math.random() * 100),
    features: [
      {
        id: Math.floor(Math.random() * 10000000),
        title: 'Size',
        value: '1209*320',
      },
      {
        id: Math.floor(Math.random() * 10000000),
        title: 'Size',
        value: '1209*320',
      },
    ],
    comments: [
      {
        name: 'Jon Doe',
        value: 'Quality Product',
      },
    ],
  },
  {
    id: 'asdkfjalkdjf9013u0123',
    imageUrl: 'aoasd',
    title: 'Product ' + Math.floor(Math.random() * 100),
    features: [
      {
        id: Math.floor(Math.random() * 10000000),
        title: 'Size',
        value: '1209*320',
      },
      {
        id: Math.floor(Math.random() * 10000000),
        title: 'Size',
        value: '1209*320',
      },
    ],
    comments: [
      {
        name: 'Jon Doe',
        value: 'Quality Product',
      },
    ],
  },
  {
    id: 'akfjalkdjf9013u0123',
    imageUrl: 'aoasd',
    title: 'Product ' + Math.floor(Math.random() * 100),
    features: [
      {
        id: Math.floor(Math.random() * 10000000),
        title: 'Size',
        value: '1209*320',
      },
      {
        id: Math.floor(Math.random() * 10000000),
        title: 'Size',
        value: '1209*320',
      },
    ],
    comments: [
      {
        name: 'Jon Doe',
        value: 'Quality Product',
      },
    ],
  },
] as IProduct[];

export const productsSlice = createSlice({
  name: 'products',
  initialState: intialProducts,
  reducers: {
    addComment: (state, {payload}) => {
      const {id, comment} = payload;
      state
        .find(p => p.id == id)
        .comments.push({
          name: Math.floor(Math.random() * 1000).toString(),
          value: comment,
        });
    },
  },
});

// Action creators are generated for each case reducer function
export const {addComment} = productsSlice.actions;

export default productsSlice.reducer;
