import React, {FC, memo, ReactNode, useState} from 'react';
import {StyleSheet, Text, TouchableWithoutFeedback, View} from 'react-native';
import {colors, scale} from '../../utils';
import {styles} from '../../utils/styles';

export const Accordion: FC<{
  title: string;
  isOpen?: boolean;
  children: ReactNode;
}> = memo(({title, isOpen, children}) => {
  const [opened, setOpened] = useState(isOpen);
  return (
    <View style={[styles.column, accordionStyles.container]}>
      <View
        style={[
          styles.row,
          {flex: 1, justifyContent: 'space-between', marginVertical: scale(10)},
        ]}>
        <Text style={[styles.textBig, styles.textBold]}>{title}</Text>
        <TouchableWithoutFeedback onPress={() => setOpened(opened => !opened)}>
          <Text style={{width: scale(30), textAlign: 'right'}}>▼</Text>
        </TouchableWithoutFeedback>
      </View>
      <View>{opened && children}</View>
    </View>
  );
});

const accordionStyles = StyleSheet.create({
  container: {
    width: '100%',
    borderTopColor: colors.gray,
    borderTopWidth: scale(2),
    marginTop: scale(10),
  },
});
