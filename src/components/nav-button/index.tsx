import React, {FC, memo} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {colors, scale} from '../../utils';
import {styles} from '../../utils/styles';
import {TextStyle, ViewStyle} from 'react-native';

export interface INavButtonProps {
  title: string;
  style?: ViewStyle;
  textStyle?: TextStyle;
  onPress: () => void;
}

export const NavButton: FC<INavButtonProps> = memo(
  ({title, style, textStyle, onPress}) => (
    <View style={buttonStyles.container}>
      <TouchableOpacity
        onPress={onPress}
        style={[style ? style : buttonStyles.roundedBtn, styles.boxShadow]}>
        <Text
          style={
            textStyle
              ? textStyle
              : [styles.textBig, styles.textBold, {alignSelf: 'center'}]
          }>
          {title}
        </Text>
      </TouchableOpacity>
    </View>
  ),
);

const buttonStyles = StyleSheet.create({
  container: {
    width: scale(294),
    height: scale(68),
    alignSelf: 'center',
    marginBottom: scale(14),
  },
  roundedBtn: {
    width: scale(283),
    height: scale(49),
    marginTop: scale(7),
    marginLeft: scale(3),
    marginRight: scale(7),
    marginBottom: scale(12),
    borderRadius: scale(29),
    backgroundColor: colors.navButtonGray,
    justifyContent: 'center',
  },
});
