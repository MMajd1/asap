import React, {FC, memo} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  InteractionManager,
} from 'react-native';
import {IProduct} from '../../types';
import {colors, constants, scale} from '../../utils';
import {styles} from '../../utils/styles';

interface IProductProps extends IProduct {
  onPress: () => void;
}

export const Product: FC<IProductProps> = memo(({title, imageUrl, onPress}) => (
  <View style={[styles.column, productStyles.container]}>
    <View style={[productStyles.imageContainer, styles.boxShadow]}>
      <TouchableOpacity
        onPress={() => {
          InteractionManager.runAfterInteractions(() => onPress());
        }}>
        <Image
          style={productStyles.image}
          source={require('../../../images/default-image.png')}
        />
      </TouchableOpacity>
    </View>
    <View style={[styles.centered, productStyles.titleContainer]}>
      <Text style={[styles.textBold, productStyles.title]}>{title}</Text>
    </View>
  </View>
));

const productStyles = StyleSheet.create({
  container: {
    width: constants.product.width,
    height: constants.product.height,
    marginTop: scale(20),
  },
  imageContainer: {
    borderRadius: scale(20),
    alignSelf: 'center',
    width: constants.product.imageContainer.width,
    height: constants.product.imageContainer.height,
    backgroundColor: colors.white,
  },
  image: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width: constants.product.image.width,
    height: constants.product.image.height,
    backgroundColor: colors.white,
  },
  titleContainer: {
    height: constants.product.textContainer.height,
  },
  title: {
    fontSize: constants.product.textContainer.fontSize,
  },
});
