import React, {FC, ReactNode} from 'react';
import {StyleProp, Text, View} from 'react-native';
import {colors, constants, scale} from '../../utils';
import {styles} from '../../utils/styles';

export const StackHeader: FC<{
  title: string;
  leftButton: ReactNode;
  style: StyleProp<{backgroundColor?: string}>;
}> = ({title, style, leftButton}) => {
  return (
    <View
      style={[
        style,
        {
          flexDirection: 'row',
          height: constants.headerHeight,
          backgroundColor: colors.headerGray,
          alignItems: 'center',
          paddingLeft: scale(10)
        },
        styles.boxShadow,
      ]}>
      {leftButton && <View style={{marginRight: scale(15)}}>{leftButton}</View>}
      <Text style={[styles.textBig, {fontWeight: 'bold'}]}>{title}</Text>
    </View>
  );
};
