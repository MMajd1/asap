import React, {FC} from 'react';
import {Text, View} from 'react-native';
import {CustomButton} from '..';
import {colors, scale} from '../../utils';
import {styles} from '../../utils/styles';

// NEED ICON HERE to ADD an ICON
interface IStackHeaderButtonProps {
  text: string;
  onPress: () => void;
}

export const StackHeaderButton: FC<IStackHeaderButtonProps> = ({
  text,
  onPress,
}) => {
  return (
    <View style={styles.column}>
      <CustomButton
        title={'<'}
        onPress={onPress}
        style={{
          backgroundColor: colors.white,
          borderWidth: scale(2),
          borderColor: colors.textGray,
          alignItems: 'center',
          justifyContent: 'center',
          width: scale(27),
          height: scale(27),
          borderRadius: scale(27 / 2),
        }}
      />
      <Text
        style={{alignSelf: 'center', fontWeight: 'bold', fontSize: scale(10)}}>
        {text}
      </Text>
    </View>
  );
};
