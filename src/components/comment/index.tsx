import React, {FC, memo} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, scale} from '../../utils';
import {styles} from '../../utils/styles';

interface ICommentProps {
  index: number;
  comment: {
    name: string;
    value: string;
  };
}

export const Comment: FC<ICommentProps> = memo(({index, comment}) => {
  const {name, value} = comment;
  return (
    <View style={commentStyles.container}>
      <Text style={commentStyles.name}>{name}</Text>
      <Text style={commentStyles.commentText}>{value}</Text>
    </View>
  );
});

const commentStyles = StyleSheet.create({
  container: {
    width: scale(291),
    marginVertical: scale(5),
  },
  name: {
    fontWeight: 'bold',
    marginTop: scale(5),
    fontSize: scale(12),
    color: colors.black,
  },
  commentText: {
    fontSize: scale(12),
  },
});
