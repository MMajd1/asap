import React, {FC, memo} from 'react';
import {Text, TextStyle, ViewStyle} from 'react-native';
import {TouchableOpacity} from 'react-native';

interface IButtonProps {
  title: string;
  onPress: () => void;
  style?: ViewStyle;
  textStyle?: TextStyle;
}

export const CustomButton: FC<IButtonProps> = memo(
  ({title, onPress, style, textStyle}) => (
    <TouchableOpacity style={style} onPress={() => onPress()}>
      <Text style={textStyle}>{title}</Text>
    </TouchableOpacity>
  ),
);
