import React, {FC, memo} from 'react';
import {Text, View} from 'react-native';
import {scale} from '../../utils';

interface IFeature {
  title: string;
  value: string;
}

export const Feature: FC<IFeature> = memo(({title, value}) => (
  <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
    <Text style={{fontSize: scale(15)}}>{title}</Text>
    <Text style={{fontSize: scale(15), fontWeight: 'bold'}}>{value}</Text>
  </View>
));
