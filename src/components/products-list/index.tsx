import React, { FC, memo } from 'react';
import { InteractionManager, StyleSheet, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { Product, Seperator } from '..';
import { navigate, setOptions } from '../../navigation';
import { IProduct } from '../../types';
import { IProductsList } from '../../types/products-list';
import { constants, routes, scale } from '../../utils';
import { styles } from '../../utils/styles';

export const ProductsList: FC<IProductsList> = memo(({ products }) => {
  return (
    <View style={[styles.container, styles.column]}>
      <FlatList
        style={{ flex: 1 }}
        data={products}
        numColumns={2}
        columnWrapperStyle={{
          justifyContent: 'space-evenly',
        }}
        ItemSeparatorComponent={() => <Seperator />}
        getItemLayout={(data, index) => ({
          length: constants.product.height,
          offset: constants.product.height * Math.floor(index / 2),
          index,
        })}
        renderItem={({ item, index }: { item: IProduct }) => (
          <Product
            key={item.id}
            title={item.title}
            onPress={() => {
              InteractionManager.runAfterInteractions(() => {
                navigate(routes.ModelDetails.name, { id: item.id });
              });
            }}
            imageUrl={item.imageUrl}
          />
        )}
      />
    </View>
  );
});

const modelsStyles = StyleSheet.create({
  container: {
    flexWrap: 'nowrap',
    justifyContent: 'space-evenly',
  },
});
