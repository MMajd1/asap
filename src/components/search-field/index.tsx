import React, {FC, memo} from 'react';
import {StyleSheet, Text, View, ViewStyle} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {CustomTextInput} from '..';
import {colors, scale, wording} from '../../utils';
import {styles} from '../../utils/styles';

interface ISearchFieldProps {
  value?: string;
  onChangeText: (text: string) => void;
  style?: ViewStyle;
}

export const SearchField: FC<ISearchFieldProps> = memo(
  ({value, onChangeText, style}) => (
    <View style={[searchFieldStyles.fieldContainer, styles.boxShadow]}>
      <CustomTextInput
        placeholder={wording.typeToSearch}
        onChangeText={onChangeText}
        style={[style, {flex: 1, height: scale(49)}]}
      />
      <View>
        <Text>ICON</Text>
      </View>
    </View>
  ),
);

const searchFieldStyles = StyleSheet.create({
  fieldContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: scale(20),
    marginHorizontal: scale(20),
    borderRadius: scale(20),
    borderWidth: 0,
    // borderColor: colors.cfGray,
    borderColor: colors.transparent,
    backgroundColor: colors.eHexGray,
    overflow: 'hidden',
  },
});
