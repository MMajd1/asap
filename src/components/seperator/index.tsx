import React, {memo} from 'react';
import {View} from 'react-native';
import {scale} from '../../utils';
import {styles} from '../../utils/styles';

export const Seperator = memo(() => (
  <View
    style={[
      styles.seperator,
      {width: '80%', marginVertical: scale(14), alignSelf: 'center'},
    ]}
  />
));
