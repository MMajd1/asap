import React, {FC, memo} from 'react';
import {
  StyleProp,
  StyleSheet,
  TextInput,
  TextStyle,
  ViewStyle,
} from 'react-native';
import {colors, scale} from '../../utils';
import {styles} from '../../utils/styles';

interface CustomInputProps {
  placeholder?: string;
  value?: string;
  onChangeText: (text: string) => void;
  style?: StyleProp<ViewStyle & TextStyle> | StyleProp<ViewStyle & TextStyle>[];
}

export const CustomTextInput: FC<CustomInputProps> = memo(
  ({placeholder, value, onChangeText, style}) => {
    return (
      <TextInput
        placeholder={placeholder}
        style={[
          {padding: 0},
          customInputStyles.input,
          value ? null : {...styles.textItalic},
          style,
        ]}
        defaultValue={value}
        onChangeText={onChangeText}
      />
    );
  },
);

const customInputStyles = StyleSheet.create({
  input: {
    paddingHorizontal: scale(10),
  },
});
