export interface INavItem {
  key: number | string;
  title: string;
}
