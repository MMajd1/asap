export interface IProduct {
  id?: number | string;
  title: string;
  imageUrl: string;
  features?: {id: number; title: string; value: string}[];
  comments?: {name: string; value: string}[];
}
