import { IProduct } from '.';

export interface IProductsList {
  products: IProduct[];
}
